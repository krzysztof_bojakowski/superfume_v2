jQuery(document).ready(()=>{
    jQuery("#owl-carousel").owlCarousel({
        autoplay:true,
        loop:true,
        dots:false,
        items:2,
    });


});

jQuery(document).ready(function() {
    setTimeout(function() {
        if (jQuery(window).width() > 500) {
            revapi1.bind("revolution.slide.onchange", function (e, data) {
                console.log(data.slideIndex);
                if (parseInt(data.slideIndex) === 1 || parseInt(data.slideIndex) === 3 ) {
                    jQuery('.header_inner li.menu-item a').css('color', 'white');
                    jQuery('.header_inner ul.sub-menu li.menu-item a').css('color', '#252525');
                } else {
                    jQuery('.header_inner li.menu-item a').css('color', '#252525');
                }
                //data.currentslide - Current  Slide as jQuery Object
                //data.prevslide - Previous Slide as jQuery Object
            });
        }
    }, 50);
    if (jQuery("#is_buttonSlider").length > 0 && jQuery(window).width() < 700) {
        jQuery("#is_buttonSlider").slick({
            arrows: true,
            infinite: true,
            slidesToShow: 1,
            autoplay: true,
            slidesToScroll: 1,
            centerPadding: '0',
            centerMode: true,
            // prevArrow: $('#prevManufacturer'),
            // nextArrow: $('#nextManufacturer'),
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
});