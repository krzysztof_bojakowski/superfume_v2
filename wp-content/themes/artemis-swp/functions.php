<?php

/*
	Load theme textdomain, editor style, auto feed links, custom background support
	Load the main stylesheet - style.css
	Load Needed Google Fonts
	Set excerpt length and Remove [...] string from excerpt
	Set the content width
*/
require_once(get_template_directory() . "/core/basic_theme_setup.php");


function is_enqueueCustomAssets()
{
    wp_enqueue_style('slickCSS', get_stylesheet_directory_uri() . '/assets/slick-1.8.1/slick.css');
    wp_enqueue_style('slickCSSTheme', get_stylesheet_directory_uri() . '/assets/slick-1.8.1/slick-theme.css');
    wp_enqueue_script('slickJS', get_stylesheet_directory_uri() . '/assets/slick-1.8.1/slick.min.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'is_enqueueCustomAssets');

/*
	Theme Settings Menu
*/
require_once(get_template_directory() . "/settings/theme_settings.php");
require_once(get_template_directory() . "/settings/settings_getters.php");

/*TODO: theme import menu*/

/*
	Set as theme Visual Composer
*/
if (function_exists("vc_set_as_theme")) {
    add_action('vc_before_init', 'ARTEMIS_SWP_vcSetAsTheme');
    function ARTEMIS_SWP_vcSetAsTheme()
    {
        vc_set_as_theme(true);
    }
}

/*
	Set as theme Slider Revolution
*/
if (function_exists('set_revslider_as_theme')) {
    add_action('init', 'ARTEMIS_SWP_RevSliderSetAsTheme');
    function ARTEMIS_SWP_RevSliderSetAsTheme()
    {
        set_revslider_as_theme();
    }
}


/*
	Utilities
*/
require_once(get_template_directory() . "/core/utils.php");

/*
	TODO: Require Lucille Demo Importer
*/
/*
	Theme Customizer
*/
require_once(get_template_directory() . "/customizer/theme_customizer.php");
/* Setup the Theme Customizer settings and controls*/
add_action('customize_register', array('ARTEMIS_SWP_Customize', 'register'));
add_action('customize_controls_enqueue_scripts',
    array('ARTEMIS_SWP_Customize', 'register_controls'));

/* Output customizer CSS to live site - customizer colors*/
add_action('wp_head', array('ARTEMIS_SWP_Customize', 'header_output'));

/* Enqueue live preview javascript in Theme Customizer admin screen*/
add_action('customize_preview_init',
    array('ARTEMIS_SWP_Customize', 'live_preview'));


/*
	Load needed js scripts and css styles
	Calls of wp_enqueue_script and wp_enqueue_style
*/
require_once(get_template_directory() . "/core/enqueue_scripts.php");


/*
	Register theme sidebars
*/
require_once(get_template_directory() . "/core/register_theme_sidebars.php");


/*
	Comments template function used as callback in wp_list_comments() call in comments.php
	Comment form defaults
*/
require_once(get_template_directory() . "/core/comments_template_cbk.php");
/*load comment reply - moved from header.php*/
if (is_singular()) {
    wp_enqueue_script('comment-reply');
}


/*
	WooCommerce related functionality
*/
require_once(get_template_directory() . "/core/woocommerce_support.php");


/*
	Checks if exists and install the required plugins that are coming with the theme
*/
require_once(get_template_directory() . "/core/install_required_plugins.php");

/*
    wp_ajax actions
*/
require_once(get_template_directory() . "/core/ajax_binding.php");

/*
	Mega Menu
*/
if (ARTEMIS_SWP_has_megamenu()) {
    require_once(get_template_directory()
                 . "/core/mega_menu/SWPFrontendWalkerNavMenu.php");
    require_once(get_template_directory() . "/core/mega_menu/SWPMegaMenu.php");
    $mega_menu_instance = new SWPMegaMenu();
}

/*
	Import/Export functionality
*/
require get_template_directory()
        . '/core/import-export/Artemis_SWP_Exporter.php';
require get_template_directory()
        . '/core/import-export/Artemis_SWP_Importer.php';

if (class_exists('Artemis_SWP_Importer')) {
    new Artemis_SWP_Importer();
}

/* by default exporter is disabled*/
define("SWP_ENABLE_DEMO_EXPORT", false);
if (SWP_ENABLE_DEMO_EXPORT && class_exists('Artemis_SWP_Exporter')) {
    new Artemis_SWP_Exporter();
}

function yourtheme_woocommerce_image_dimensions()
{
    $single = array(
        'width'  => '500',   // px
        'height' => '600',   // px
        'crop'   => 0        // true
    );

    update_option('shop_single_image_size', $single);
}

// Hook in
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields($fields)
{
    $fields['billing']['billing_nip'] = array(
        'type'        => 'text',
        'placeholder' => _x('NIP', 'placeholder', 'woocommerce'),
        'required'    => false,
        'class'       => array('form-row-wide'),
        'clear'       => true
    );

    return $fields;
}

add_action('woocommerce_register_post', 'wooc_validate_extra_register_fields',
    10, 3);
function wooc_save_extra_register_fields($customer_id)
{
    if (isset($_POST['billing_nip'])) {
        update_user_meta($customer_id, 'billing_nip',
            sanitize_text_field($_POST['billing_nip']));
    }
}

add_filter('woocommerce_checkout_fields', 'changeWooCommerceCheckoutFieldsOrder');
function changeWooCommerceCheckoutFieldsOrder($checkout_fields)
{
    $checkout_fields['billing']['billing_first_name']['priority'] = 1;
    $checkout_fields['billing']['billing_last_name']['priority']  = 2;
    $checkout_fields['billing']['billing_company']['priority']    = 3;
    $checkout_fields['billing']['billing_nip']['priority']        = 4;

    return $checkout_fields;
}

add_filter('woocommerce_order_formatted_billing_address', 'changeFormattedBillingsAddress', 10, 2);
function changeFormattedBillingsAddress($address, $wc_order)
{
    $address = array(
        'first_name' => $wc_order->billing_first_name . "\n" . $wc_order->billing_last_name,
        'last_name'  => $wc_order->billing_last_name,
        'company'    => $wc_order->billing_company,
        'nip'        => $wc_order->billing_nip,
        'address_1'  => $wc_order->billing_address_1,
        'address_2'  => $wc_order->billing_address_2,
        'city'       => $wc_order->billing_city,
        'postcode'   => $wc_order->billing_postcode,
        'state'      => $wc_order->billing_state,
        'country'    => $wc_order->billing_country
    );

    return $address;
}

add_action('woocommerce_order_details_after_customer_details',
    'addExtraOrderCustomerDetails');
function addExtraOrderCustomerDetails($order)
{
    $order      = wc_get_order($order->id);
    $billingNip = $order->get_meta('billing_nip');
    ?>
    <tr>
        <th><?php echo ('NIP'); ?></th>
        <td><?php echo esc_html($billingNip); ?></td>
    </tr>
    <?php
}


add_filter('woocommerce_after_edit_address_form_billing', 'add_nip_field_func',
    10, 1);
function add_nip_field_func()
{
    $current_user = wp_get_current_user();
    $nip          = get_user_meta($current_user->ID, 'billing_nip', true);
    ?>
    <p class="form-row form-row form-row-wide"
       id="billing_nip_edit_processed_by_init_hook">
        <label for="billing_nip_edit_processed_by_init_hook"
               class="">
            <?php echo ('NIP (Wymaga nazwy firmy)'); ?>
        </label>
        <input class="input-text" name="billing_nip_edit_processed_by_init_hook"
               id="billing_nip_edit_processed_by_init_hook"
               value="<?php echo $nip; ?>" type="text">
    </p>
    <?php
}

add_action( 'woocommerce_checkout_update_order_meta', 'wpdesk_checkout_vat_number_update_order_meta' );
/**
 * Save VAT Number in the order meta
 */
function wpdesk_checkout_vat_number_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['billing_nip'] ) ) {
        update_post_meta( $order_id, 'billing_nip', sanitize_text_field( $_POST['billing_nip'] ) );
    }
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'wpdesk_vat_number_display_admin_order_meta', 10, 1 );
/**
 * Wyświetlenie pola NIP
 */
function wpdesk_vat_number_display_admin_order_meta( $order ) {
    echo '<p><strong>' . __( 'NIP', 'woocommerce' ) . ':</strong> ' . get_post_meta( $order->id, 'billing_nip', true ) . '</p>';
}



