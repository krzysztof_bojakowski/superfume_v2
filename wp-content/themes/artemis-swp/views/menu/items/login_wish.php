<?php if (ARTEMIS_SWP_has_wishlist_on_menu()) { ?>
    <div class="at_login_wish" style="display: flex;align-items: center;">

      <?php if ( ARTEMIS_SWP_is_woocommerce_active() ) { ?>

          <?php if ( is_user_logged_in() ) { ?>
            <a href="<?php echo esc_attr( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>"
               class="at_to_my_account"
               title="<?php esc_attr_e( 'Mój profil', 'artemis-swp' ); ?>">
                <span class="lnr lnr-user" style="height: 90px;line-height: 90px;z-index: 11;font-weight: normal;"></span>
            </a>
        <?php } else { ?>
              <a href="<?php echo esc_attr( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>"
                 class="at_to_my_account"
                 title="<?php esc_attr_e( 'Mój profil', 'artemis-swp' ); ?>">
                  <span class="lnr lnr-user" style="height: 90px;line-height: 90px;z-index: 11;font-weight: normal;"></span>
              </a>
        <?php } ?>
      <?php } ?>
    </div>
<?php } ?>